# Lenguajes de programación y algoritmos

## 1. Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)


```plantuml
@startmindmap
* Programando ordenadores \nen los 80 y ahora 
 
 * Programacion
  * Antes
   *_ Se utilizaba principalmente
    * Ensamblador
     *_ Debido a los
      * Recursos limitados
     *_ Era necesario conocer la
      * Arquitectura
       *_ Ya que cada una tenia su propio
        * Set de instrucciones
         *_ Para
          * Mayor aprovechamiento de los recursos 
        *_ Diferentes
         * Registros de memoria

  * Ahora
   *_ Se cumple la
    * Ley de Moore
     *_ Se utilizan lenguajes de
      * Alto nivel
       *_ El codigo es
        * Portable
         *_ Existen infinidad de
          * Librerias
           *_ Su uso excesivo genera
            * Ineficiencia
             *_ Se cumple la
              * Ley de Wirth      
       *_ Cuenta con varias
        * Capas o niveles de abstraccion
         *_ Se prioriza la
          * Legibilidad del codigo
           *_ Sobre el 
            * Rendimiento
@endmindmap 
```

## 2. Hª de los algoritmos y de los lenguajes de programación (2010)

```plantuml
@startmindmap
* Hª de los algoritmos \ny de los lenguajes de programación 
 
 * Algoritmos
  *_ Se utilizan desde hace
   * 3000 años
    *_ es un
     * Procedimiento sistematico
      *_ Con una
       * Serie finita de pasos
        *_ Para resolver un
         * Problema especifico
    *_ Clasifican de acuerdo a su
     * Complejidad
      *_ estan los
       * Razonables o Polinomiales        
      *_ y los
       * No razonables o Exponenciales

 * Maquinas Programables
  *_ Aparecen
   * Primeras calculadoras mecanicas
    *_ En el
     * Siglo XVII      
  *_ Aparece la primera
   * Maquina analitica
    *_ En el
     * Siglo XIX
      *_ Desarrollada por
       * Charles Babbage
    *_ Fue el primer diseño de un
     * Computador moderno

 * Lenguajes de programación
  *_ Existen
   * Paradigmas
    * Imperativo
     *_ Como
      * Fortran 1956
    * Funcional
     *_ Como
      * LISP 1958
    * POO
     *_ Como
      * Simula 1962
    * Logico
     *_ Como
      * Prolog 1972
    * Concurrente

@endmindmap 
```

## 3. Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)

```plantuml
@startmindmap
* Tendencias actuales en los lenguajes de \nProgramación y Sistemas Informáticos. \nLa evolución de los lenguajes \ny paradigmas de programación. 

 * Herramientas
  *_ Se utiliza
   * Ordenador
    *_ Codificado por un
     * Programador
      *_ Utilizando
       * Lenguajes de programación
        *_ De acuerdo a las
         * Necesidades del problema \n o proyecto
          *_ Actualmente existe varios
           * Paradigmas

            * Programacion estructurada
             * Abstraccion de datos
             * Estructuras de control
             * Secuencial
             * Principales Lenguajes
              * C
              * Basic
              * Pascal

            * Programacion funcional
             * Expresiones matematicas
             * No secuencial
             * No estructuras de control
             * Principales Lenguajes
              * Haskell
              * Lisp
              * Scala

            * Programacion logica
             * Expresiones logicas
             * Cálculo lambda
             * Logica de primer oden
             * Principales Lenguajes
              * Prolog

        *_ Y para 
         * Automatizar o facilitar
          * Tareas o actividades 

@endmindmap 
```